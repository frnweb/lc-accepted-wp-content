<?php
//
// Recommended way to include parent theme styles.
//  (Please see http://codex.wordpress.org/Child_Themes#How_to_Create_a_Child_Theme)
//  
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}
//
// Your code goes below
//

// Begin Removing RSS Feeds for comments while keeping the main one available
// First, we remove all the RSS feed links from wp_head using remove_action
  remove_action( 'wp_head','feed_links', 2 );
  remove_action( 'wp_head','feed_links_extra', 3 );
  
  // We then need to reinsert the main RSS feed by using add_action to call our function
  add_action( 'wp_head', 'reinsert_rss_feed', 1 );
 
  // This function will reinsert the main RSS feed *after* the others have been removed
  function reinsert_rss_feed() {
      echo '<link rel="alternate" type="application/rss+xml" title="' . get_bloginfo('sitename') . ' &raquo; RSS Feed" href="' . get_bloginfo('rss2_url') . '" />';
  }
  //

//
// Changes Excerpt Length
//
    add_filter( 'excerpt_length', 'woo_custom_excerpt_length', 200 );
 
    function woo_custom_excerpt_length ( $length ) {
        if ( is_front_page() ) { $length = 80; }
        return $length;
    } // End woo_custom_excerpt_length()


//
// Allows pinch & zoom on iphones/ipad
//
function woo_load_responsive_meta_tags () {
		$html = '';
		
		$html .= "\n" . '<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->' . "\n";
		$html .= '<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />' . "\n";
		
		/* Remove this if not responsive design */
		$html .= "\n" . '<!--  Mobile viewport scale | Disable user zooming as the layout is optimised -->' . "\n";
		$html .= '<meta content="initial-scale=1.0; maximum-scale=4.0; user-scalable=yes" name="viewport"/>' . "\n";

		echo $html;
	} // End woo_load_responsive_meta_tags()

	

	
	
//////
// AUTHOR/USER CUSTOMIZATIONS
//////

//Add more contact options to users
function modify_contact_methods($profile_fields) {
	// Add new contact fields
	$profile_fields['linkedin'] = 'LinkedIn URL';
	$profile_fields['slideshare'] = 'SlideShare Profile';
	// Remove old contact fields
	unset($profile_fields['aim']);
	unset($profile_fields['yim']);
	unset($profile_fields['jabber']);
	
	return $profile_fields;
}
add_filter('user_contactmethods', 'modify_contact_methods');

remove_filter('pre_user_description', 'wp_filter_kses'); //removes HTML filter from author bios
add_filter( 'pre_user_description', 'wp_filter_post_kses' );  //adds traditional filters to bios like page content has


// BEGIN: Adds a FRN author settings section at the bottom of user profile settings
function add_frn_author_fields( $user ) {
    $position = esc_attr( get_the_author_meta( 'position', $user->ID ) );
	$company = esc_attr( get_the_author_meta( 'company', $user->ID ) );
	$author_public = get_the_author_meta( 'author_public', $user->ID);
?>
    <h3><?php _e('FRN Author Information'); ?></h3>
    <table class="form-table">
        <tr>
            <th><label for="Position Title"><?php _e('Position Title'); ?></label></th>
            <td>
              <input type="text" name="position" id="position" value="<?php echo $position; ?>" class="regular-text" /><span class="description"><?php _e(''); ?></span>
            </td>
        </tr>
        <tr>
            <th><label for="Company"><?php _e('Facility/Company'); ?></label></th>
            <td>
              <input type="text" name="company" id="company" value="<?php echo $company; ?>" class="regular-text" /><span class="description"><?php _e(''); ?></span>
            </td>
        </tr>
		<tr>
            <th><label for="Make Author Public"><?php _e('Make Author Public'); ?></label></th>
            <td>
				<label><input type="checkbox" name="author_public" <?php if ($author_public == 'Y' ) { ?>checked="checked"<?php }?> value="Y">Yes &nbsp;</label><small class="description"><?php _e('(Activates authorship in posts & pages)'); ?></small>
            </td>
        </tr>
    </table>
<?php 
}
function frn_custom_user_profile_fields( $user_id ) {
    if ( !current_user_can( 'edit_user', $user_id ) )
        return FALSE;
	update_usermeta( $user_id, 'position', $_POST['position'] );
    update_usermeta( $user_id, 'company', $_POST['company'] );
	update_usermeta( $user_id, 'author_public', $_POST['author_public'] );
}

add_action( 'show_user_profile', 'add_frn_author_fields' );
add_action( 'edit_user_profile', 'add_frn_author_fields' );
add_action( 'personal_options_update', 'frn_custom_user_profile_fields' );
add_action( 'edit_user_profile_update', 'frn_custom_user_profile_fields' );

//END: FRN Author fields


/*
function change_author_permalinks() {
    global $wp_rewrite;
    $wp_rewrite->author_base = 'author/';
    $wp_rewrite->author_structure = '/' . $wp_rewrite->author_base. '%author%';
}
add_action('init','change_author_permalinks');
*/



////////
// END AUTHOR/USER CUSTOMIZATIONS
////////