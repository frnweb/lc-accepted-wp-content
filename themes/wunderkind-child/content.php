<?php 
global $st_textdomain;
if(get_post_type()!='portfolio')

{ ?>


    <h2 itemprop="name"  class="post-title"><a href="<?php echo the_permalink()?>" ><?php the_title()?></a></h2>

    <p itemprop="datePublished"><span class="icon ion-ios7-clock"></span> <?php echo __('Posted on',$st_textdomain)?> <?php echo the_date('F d, Y \a\t H:i A') ?></p>



<?php

}



the_content();


?>