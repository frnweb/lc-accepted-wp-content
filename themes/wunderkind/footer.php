
<footer id="footer">
    <div class="col-lg-12 text-center">
        <div class="back-to-top">
            <i class="fa fa-angle-double-up"></i>
        </div>
    </div>

    <div class="container text-center">
        <div class="row">
            <?php 
            if(st_get_option('footer_style'))
            {
                echo st_load_template('footer/footer',st_get_option('footer_style'));
            }
            else {
                echo st_load_template('footer/footer');
            } ?>
        </div>
    </div>
</footer>


<?php if( is_page() and get_post_meta(get_the_ID(),'_cmb_fullwidth',true ) == 'boxed'){
    echo "</div><!--End Container Wrap-->";
} ?>

<?php wp_footer()?>

</body>